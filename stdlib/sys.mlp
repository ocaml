(***********************************************************************)
(*                                                                     *)
(*                           Objective Caml                            *)
(*                                                                     *)
(*            Xavier Leroy, projet Cristal, INRIA Rocquencourt         *)
(*                                                                     *)
(*  Copyright 1996 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU Library General Public License, with    *)
(*  the special exception on linking described in file ../LICENSE.     *)
(*                                                                     *)
(* Portions (C) Edgar Friendly <thelema314@gmail.com>                  *)
(***********************************************************************)

(* $Id$ *)

(* WARNING: sys.ml is generated from sys.mlp.  DO NOT EDIT sys.ml or
   your changes will be lost.
*)

(* System interface *)

external get_config: unit -> string * int = "caml_sys_get_config"
external get_argv: unit -> string * string array = "caml_sys_get_argv"

let (executable_name, argv) = get_argv()
let (os_type, word_size) = get_config()
let max_array_length = (1 lsl (word_size - 10)) - 1;;
let max_string_length = word_size / 8 * max_array_length - 1;;

external file_exists: string -> bool = "caml_sys_file_exists"
external is_directory : string -> bool = "caml_sys_is_directory"
external remove: string -> unit = "caml_sys_remove"
external rename : string -> string -> unit = "caml_sys_rename"
external getenv: string -> string = "caml_sys_getenv"
external command: string -> int = "caml_sys_system_command"
external time: unit -> float = "caml_sys_time"

let time_f f x = 
  let t0 = time () in
  let fx = f x in
  (time() -. t0, fx)

external chdir: string -> unit = "caml_sys_chdir"
external getcwd: unit -> string = "caml_sys_getcwd"
external readdir : string -> string array = "caml_sys_read_directory"

let interactive = ref false

type signal_behavior =
    Signal_default
  | Signal_ignore
  | Signal_handle of (int -> unit)

external signal : int -> signal_behavior -> signal_behavior
                = "caml_install_signal_handler"

let set_signal sig_num sig_beh = ignore(signal sig_num sig_beh)

let sigabrt = -1
let sigalrm = -2
let sigfpe = -3
let sighup = -4
let sigill = -5
let sigint = -6
let sigkill = -7
let sigpipe = -8
let sigquit = -9
let sigsegv = -10
let sigterm = -11
let sigusr1 = -12
let sigusr2 = -13
let sigchld = -14
let sigcont = -15
let sigstop = -16
let sigtstp = -17
let sigttin = -18
let sigttou = -19
let sigvtalrm = -20
let sigprof = -21

module Signal = struct
  type t = SIGABRT | SIGALRM | SIGFPE | SIGHUP | SIGILL | SIGINT | SIGKILL | SIGPIPE | SIGQUIT | SIGSEGV | SIGTERM | SIGUSR1 | SIGUSR2 | SIGCHLD | SIGCONT | SIGSTOP | SIGTSTP | SIGTTIN | SIGTTOU | SIGVTALRM | SIGPROF | SIGOther of int

  let of_int = function -1 -> SIGABRT | -2 -> SIGALRM | -3 -> SIGFPE | -4 -> SIGHUP | -5 -> SIGILL | -6 -> SIGINT | -7 -> SIGKILL | -8 -> SIGPIPE | -9 -> SIGQUIT | -10 -> SIGSEGV | -11 -> SIGTERM | -12 -> SIGUSR1 | -13 -> SIGUSR2 | -14 -> SIGCHLD | -15 -> SIGCONT | -16 -> SIGSTOP | -17 -> SIGTSTP | -18 -> SIGTTIN | -19 -> SIGTTOU | -20 -> SIGVTALRM | -21 -> SIGPROF | x -> SIGOther x
    
  let to_int = function SIGABRT -> -1 | SIGALRM -> -2 | SIGFPE -> -3 | SIGHUP -> -4 | SIGILL -> -5 | SIGINT -> -6 | SIGKILL -> -7 | SIGPIPE -> -8 | SIGQUIT -> -9 | SIGSEGV -> -10 | SIGTERM -> -11 | SIGUSR1 -> -12 | SIGUSR2 -> -13 | SIGCHLD -> -14 | SIGCONT -> -15 | SIGSTOP -> -16 | SIGTSTP -> -17 | SIGTTIN -> -18 | SIGTTOU -> -19 | SIGVTALRM -> -20 | SIGPROF -> -21 | SIGOther x -> x
    
  let set_signal sig_var sig_beh = ignore(signal (to_int sig_var) sig_beh)
end

exception Break

let catch_break on =
  if on then
    set_signal sigint (Signal_handle(fun _ -> raise Break))
  else
    set_signal sigint Signal_default


(* The version string is found in file ../VERSION *)

let ocaml_version = "%%VERSION%%";;
